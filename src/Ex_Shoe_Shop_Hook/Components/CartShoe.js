import React from 'react'

export default function CartShoe({cart, handleDelete, handleChangeQuality}) {
  let renderTbody = () => {
    return cart.map((item) => {
        return (
            <tr key={item.id}>
                <td>{item.id}</td>
                <td>{item.name}</td>
                <td>
                    <img src={item.image} alt="" style={{width: 50}}/>    
                </td>
                <td>{item.price}</td>
                <td>
                    <button onClick={() => handleChangeQuality(item.id, -1)} className='btn px-1 mx-1 quantityButton'>-</button>
                    <strong>{item.soLuong}</strong>    
                    <button onClick={() => handleChangeQuality(item.id, 1)} className='btn px-1 mx-1 quantityButton'>+</button>    
                </td>
                <td>{item.price * item.soLuong}</td>
                <td>
                    <button onClick={() => handleDelete(item)} className='btn btn-danger'>Delete</button>
                </td>
            </tr>
        )
    })
  }
  return (
    <div className='cartShoe'>
        <table className="table">
            <thead>
                <tr>
                    <th>ID</th>
                    <th>Name</th>
                    <th>Image</th>
                    <th>Unit price</th>
                    <th>Quantity</th>
                    <th>Total price</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>{renderTbody()}</tbody>
        </table>
    </div>
  )
}
