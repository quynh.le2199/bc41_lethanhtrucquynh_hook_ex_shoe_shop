import React, {useState} from 'react'
import { data_shoe } from '../data_shoe'
import ListShoe from './ListShoe';
import CartShoe from './CartShoe';

export default function Ex_Shoe_Shop_Hook() {
  const [dataShoe, setDataShoe] = useState(data_shoe);
  const [cart, setCart] = useState([]);
  
  let handleAddToCart = (shoe) => {
    let cloneCart = [...cart];
    let index = cloneCart.findIndex((item) => {
        return item.id == shoe.id;
    })
    if (index == -1) {
        let newShoe = {...shoe, soLuong: 1};
        cloneCart.push(newShoe);
    }
    else {
        cloneCart[index].soLuong++;
    }
    setCart(cloneCart);
  }

  let handleDelete = (shoe) => {
    let newCart = cart.filter((item) => {
      return item.id != shoe.id;
    })
    setCart(newCart);
  }

  let handleChangeQuality = (idShoe, payload) => {
    let cloneCart = [...cart];
    let index = cloneCart.findIndex((item) => {
      return item.id == idShoe;
    })
    cloneCart[index].soLuong += payload;
    let newCart = cloneCart.filter((item) => {
      return item.soLuong > 0
    });
    setCart(newCart);
  }

  return (
    <div className='container'>
        <h2 className='my-3'>Ex Shoe Shop Hook</h2>
        {cart.length >0 && <CartShoe cart={cart} handleDelete={handleDelete} handleChangeQuality={handleChangeQuality}/>}
        <ListShoe data={dataShoe} handleAddToCart={handleAddToCart}/>
    </div>
  )
}
