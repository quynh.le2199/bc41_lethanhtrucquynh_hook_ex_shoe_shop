import React from 'react'
import ItemShoe from './ItemShoe'

export default function ListShoe({data, handleAddToCart}) {
  let renderListShoe = () => {
    return data.map((item) => {
        return <ItemShoe shoe={item} key={item.id} handleAddToCart={handleAddToCart}/>
    })
  }
  return (
    <div className='row'>{renderListShoe()}</div>
  )
}
