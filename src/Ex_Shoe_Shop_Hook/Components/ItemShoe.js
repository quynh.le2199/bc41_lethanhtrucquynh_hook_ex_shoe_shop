import React from 'react'

export default function ItemShoe({shoe, handleAddToCart}) {
  return (
    <div className='col-4 p-3 shoeItem'>
        <div className="card border-primary px-2">
            <img className="card-img-top" src={shoe.image}/>
            <div className="card-body">
                <h5 className="card-title">{shoe.name}</h5>
                <p className="card-text itemPrice">${shoe.price}</p>
                <br />
                <button onClick={() => handleAddToCart(shoe)} className='btn btn-primary'>Add to cart</button>
            </div>
        </div>
   
    </div>
  )
}
